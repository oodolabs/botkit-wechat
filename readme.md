# botkit-wechat [![Build Status](https://travis-ci.org/taoyuan/botkit-wechat.svg?branch=master)](https://travis-ci.org/taoyuan/botkit-wechat)

> WeChat Connector for Botkit


## Install

```
$ npm install botkit-wechat
```


## Usage

See [examples/wechat.js](examples/wechat.js)
