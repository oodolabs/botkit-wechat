const _ = require('lodash');
const botkit = require('botkit');
const Worker = require('./worker');

/**
 *
 * @param {Object} [config]
 * @returns {WeChatController}
 */
module.exports = function (config) {
  const wxbotkit = botkit.core(config || {});

  wxbotkit.defineBot(Worker);

  // universal normalizing steps
  // handle normal messages from users (text, stickers, files, etc count!)
  wxbotkit.middleware.normalize.use((bot, message, next) => {
    // const {raw} = message;

    const from = normalizeUser(message.FromUserName, bot);
    let {sender = from, text} = decodeContent(message.OriginalContent, bot);
    message.from = from;
    message.sender = sender;
    message.text = text;
    message.id = message.MsgId;
    // capture the user ID
    message.user = sender.id;
    // since there are only 1:1 channels on WeChat, the channel id is set to the user id
    message.channel = from.id;
    next();
  });

  // handle message sub-types
  wxbotkit.middleware.categorize.use(function handleOptIn(bot, message, next) {

    const MENTION = '@' + bot.identity.id;
    const RX_MENTION = new RegExp(MENTION, 'i');
    const RX_DIRECT_MENTION = new RegExp('^' + MENTION, 'i');

    if (message.text) {
      message.text = message.text.trim();
    }

    message.mediatype = bot.toMediaType(message.MsgType, message.AppMsgType);

    //
    if (message.mediatype === 'file') {
      message.mediaid = message.MediaId;
      message.filename = message.FileName;
    }

    message.type = message.mediatype;

    next();
  });

  wxbotkit.middleware.format.use(function (bot, message, platform_message, next) {
    // clone the incoming message
    Object.assign(platform_message, message);
    next();
  });

  return wxbotkit;
};

function normalizeUser(id, bot) {
  const c = bot.contacts[id];
  if (id === bot.identity.id) {
    return _.pick(bot.identity, ['id', 'name']);
  }
  return {
    id,
    name: c && c.getDisplayName(),
    remark: _.get(c, 'RemarkName')
  }
}

function decodeContent(content, bot) {
  const RX_TEXT = /^(@.*):(.*)/g;
  let answer = {text: content};
  if (content) {
    const m = RX_TEXT.exec(content);
    if (m) {
      answer = {
        sender: normalizeUser(m[1], bot),
        text: m[2]
      }
    }
    answer.text = answer.text.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/<br\/>/g, '\n');
  }
  return answer;
}
