import {Controller, Bot, Message, Configuration} from "botkit";

export = wechat;

declare function wechat(config: wechat.WeChatConfiguration): wechat.WeChatController;

declare namespace wechat {

  export interface WeChatUser {
    id: string;
    name: string;
    remark?: string;
  }

  export interface WeChatSpawnConfiguration {
    utterances?: {
      yes: RegExp;
      no: RegExp;
      quit: RegExp;
    };
    session?: string;
  }

  export interface WeChatConfiguration extends Configuration {
  }

  export interface WeChatMessage extends Message {
    id: string;
    from: WeChatUser;
    sender: WeChatUser;
    mediatype?: string;
    mediaid?: string;
    filename?: string;
  }

  export interface Media {
    type: string;
    data: Buffer;
  }

  export interface WeChatBot extends Bot<WeChatSpawnConfiguration, WeChatMessage> {
    start(): this;
    toMediaType(code: number): string;
    fetchMedia(message: WeChatMessage): Promise<Media>;
  }

  export interface WeChatController extends Controller<WeChatConfiguration, WeChatMessage, WeChatBot> {
  }

}
