module.exports = {
  "yes": /^(yes|yea|yup|yep|ya|sure|ok|y|yeah|yah|是|好|可以|行|对|没错|没问题)/i,
  "no": /^(no|nah|nope|n|不|不用|不了|不是|不好|不可以|不行|不对|不要)/i,
  "quit": /^(quit|cancel|end|stop|done|exit|nevermind|never mind|退出|离开|结束|取消|停止|停|完成)/i
};
