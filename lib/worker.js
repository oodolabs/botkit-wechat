const fs = require('fs-extra');
const path = require('path');
const PromiseA = require('bluebird');
const EventEmitter = require('events').EventEmitter;
const WeChat = require('wechatr').WeChat;
const CONF = require('wechatr/lib/util/conf').CONF;
const UTTERANCES = require('./utterances');

const MSG_TYPES = {
  [CONF.MSGTYPE_TEXT]: 'text',
  [CONF.MSGTYPE_IMAGE]: 'image',
  [CONF.MSGTYPE_VOICE]: 'voice',
  [CONF.MSGTYPE_VIDEO]: 'video',
  [CONF.MSGTYPE_MICROVIDEO]: 'video',
  [CONF.MSGTYPE_EMOTICON]: 'emoticon',
  [CONF.MSGTYPE_APP]: 'app',
  [CONF.MSGTYPE_VOIPMSG]: 'voip',
  [CONF.MSGTYPE_VOIPNOTIFY]: 'voip_notify',
  [CONF.MSGTYPE_VOIPINVITE]: 'voip_invite',
  [CONF.MSGTYPE_LOCATION]: 'location',
  [CONF.MSGTYPE_STATUSNOTIFY]: 'status_notify',
  [CONF.MSGTYPE_SYSNOTICE]: 'sys_notice',
  [CONF.MSGTYPE_POSSIBLEFRIEND_MSG]: 'possible_friend',
  [CONF.MSGTYPE_VERIFYMSG]: 'verify',
  [CONF.MSGTYPE_SHARECARD]: 'sharecard',
  [CONF.MSGTYPE_SYS]: 'sys',
  [CONF.MSGTYPE_RECALLED]: 'recalled',
};

const APP_MSG_TYPES = {
  [CONF.APPMSGTYPE_TEXT]: 'text',
  [CONF.APPMSGTYPE_IMG]: 'img',
  [CONF.APPMSGTYPE_AUDIO]: 'audio',
  [CONF.APPMSGTYPE_VIDEO]: 'video',
  [CONF.APPMSGTYPE_URL]: 'url',
  [CONF.APPMSGTYPE_ATTACH]: 'attach',
  [CONF.APPMSGTYPE_OPEN]: 'open',
  [CONF.APPMSGTYPE_EMOJI]: 'emoji',
  [CONF.APPMSGTYPE_VOICE_REMIND]: 'voice_remind',
  [CONF.APPMSGTYPE_SCAN_GOOD]: 'scan_good',
  [CONF.APPMSGTYPE_GOOD]: 'good',
  [CONF.APPMSGTYPE_EMOTION]: 'emotion',
  [CONF.APPMSGTYPE_CARD_TICKET]: 'card_ticket',
  [CONF.APPMSGTYPE_REALTIME_SHARE_LOCATION]: 'realtime_share_location',
  [CONF.APPMSGTYPE_TRANSFERS]: 'transfers',
  [CONF.APPMSGTYPE_RED_ENVELOPES]: 'red_envelopes',
  [CONF.APPMSGTYPE_READER_TYPE]: 'reader_type',
  [CONF.UPLOAD_MEDIA_TYPE_IMAGE]: 'upload_media_type_image',
};

/**
 * @typedef {Object} IncomingMessage
 * @property {String} channel
 * @property {String} user
 * @property {String} text
 * @property {String} type
 * @property {String} mediatype
 * @property {String} mediatid
 * @property {String} filename
 */

/**
 * @typedef {Object} OutgoingMessage
 * @property {String} channel The channel to send to
 * @property {String} to The user to send to
 * @property {String} user The alternative property for "to"
 * @property {String} text The text message to send
 * @property {String} emoticonMd5 The emotion icon md5 string to send
 * @property {String} filename The file name to send
 * @property {Buffer|Stream|File|Blob} file The file data to send
 */

/**
 *
 */
class Bot extends EventEmitter {
  /**
   *
   * @param botkit
   * @param {Object} [config]
   * @param {Object} [config.utterances]
   * @param {String} [config.session]
   */
  constructor(botkit, config) {
    super();
    this.config = config = Object.assign({
      utterances: UTTERANCES,
      session: path.resolve('./session.json')
    }, config);

    this.type = 'wechat';
    this.msgcount = 0;
    this.botkit = botkit;
    this.utterances = config.utterances;
    this.identity = {
      id: null,
      name: '',
    };
  }

  get contacts() {
    return this.client.contacts;
  }

  _loadSession() {
    if (this.config.session) {
      try {
        return fs.readJSONSync(this.config.session);
      } catch (e) {
      }
    }
  }

  _saveSession(session) {
    if (this.config.session) {
      const dir = path.dirname(this.config.session);
      fs.ensureDirSync(dir);
      fs.writeJSONSync(this.config.session, session);
    }
  }

  _deleteSession() {
    if (fs.existsSync(this.config.session) && fs.statSync(this.config.session).isFile()) {
      fs.unlinkSync(this.config.session);
    }
  }

  start(login) {
    const {botkit} = this;
    let client = this.client;
    if (client && client.state === client.CONF.STATE.login) {
      return client;
    }
    client = this.client = new WeChat();

    client.on('uuid', (uuid, url) => {
      botkit.log('uuid', uuid, url);
      login && login(uuid, url);
      this.emit('uuid', uuid, url);
    });

    client.on('login', () => {
      this.identity = {
        id: client.session.user.UserName,
        name: client.session.user.NickName
      };
      botkit.startTicking();
      botkit.log('login', this.identity);
      this._saveSession(client.session);
    });

    client.on('logout', () => {
      botkit.log('logout');
      this.identity = {
        id: null,
        name: ''
      };
      this._deleteSession();
    });

    // client.on('contacts-updated', contacts => {
    //   console.log('联系人数量：', Object.keys(this.contacts).length);
    //   console.log(contacts)
    // });

    client.on('message', message => {
      botkit.ingest(this, message, client);
    });

    client.on('error', err => {
      console.error(err);
    });

    client.start(this._loadSession());
    return this;
  }

  toMediaType(msgType, appType) {
    let answer = MSG_TYPES[msgType] || msgType;
    if (answer === 'app') {
      if (APP_MSG_TYPES[appType] === 'attach') {
        answer = 'file'
      }
    }
    return answer;
  }

  retrieveMedia(message) {
    if (message.mediatype === 'image') {
      return this.client.getMsgImg(message.id);
    } else if (message.mediatype === 'voice') {
      return this.client.getVoice(message.id);
    } else if (message.mediatype === 'emoticon') {
      return this.client.getMsgImg(message.id);
    } else if (message.mediatype === 'video') {
      return this.client.getVideo(message.id);
    } else if (message.mediatype === 'file') {
      return this.client.getDoc(message.from.id, message.mediaid, message.filename);
    }
    return PromiseA.resolve();
  }

  findConversation(message, callback) {
    this.botkit.debug('CUSTOM FIND CONVO', message.user, message.channel);
    for (let task of this.botkit.tasks) {
      for (let convo of task.convos) {
        if (convo.isActive()
          && convo.source_message.channel === message.channel) {
          this.botkit.debug('FOUND EXISTING CONVO!');
          callback(convo);
          return
        }
      }
    }
    callback();
  }

  /**
   * Reply to incoming message
   *
   * @param {IncomingMessage} src
   * @param {OutgoingMessage|String} resp
   * @param {Function} [cb]
   * @returns {*}
   */
  reply(src, resp, cb) {
    let msg = {};

    if (typeof resp === 'string') {
      msg.text = resp;
    } else {
      msg = resp;
    }

    msg.channel = src.channel;
    msg.to = src.user;

    return PromiseA.fromCallback(cb => this.say(msg, cb)).asCallback(cb);
  }

  /**
   * Send message to user or channel
   *
   * @param {OutgoingMessage} message
   * @param {Function} [cb]
   */
  send(message, cb) {
    this.botkit.debug('SAY', message);
    this.msgcount++;
    let to = message.to || message.user || message.channel;
    if (to === this.identity.id) {
      to = 'filehelper';
    }
    return PromiseA.resolve(this.client.sendMsg(message, to)).asCallback(cb);
  }
}

module.exports = Bot;
