const qrcode = require('qrcode-terminal');
const wechat = require('../lib/wechat');

const controller = wechat();

const bot = controller.spawn().start((uuid, url) => {
  qrcode.generate(url, {small: true});
});

controller.hears(['^hello', '^hi', '^你好'], 'text', function (bot, message) {
  controller.storage.users.get(message.user, function (err, user) {
    if (user && user.name) {
      bot.reply(message, '你好，' + user.name + '!!');
    } else {
      bot.reply(message, '你好！');
    }
  });
});

controller.hears(['^我叫(.*)'], 'text', function (bot, message) {
  const name = message.match[1];
  controller.storage.users.get(message.user, function (err, user) {
    if (!user) {
      user = {
        id: message.user,
      };
    }
    user.name = name;
    controller.storage.users.save(user, function (err, id) {
      bot.reply(message, user.name + '啊，我记住啦');
    });
  });
});

controller.hears(['^我的名字', '^我是谁'], 'text', function (bot, message) {
  controller.storage.users.get(message.sender.id, function (err, user) {
    if (user && user.name) {
      bot.reply(message, '你的名字是：' + user.name);
    } else {
      bot.reply(message, '我还不知道！');
    }
  });
});

controller.hears(['^关闭'], 'text', function (bot, message) {
  bot.startConversation(message, function (err, convo) {
    convo.ask('你确定要把我关了吗?', [{
      pattern: bot.utterances.yes,
      callback: function (response, convo) {
        convo.say('再见!👋');
        convo.next();
        setTimeout(function () {
          process.exit();
        }, 3000);
      }
    }, {
      pattern: bot.utterances.no,
      default: true,
      callback: function (response, convo) {
        convo.say('哦😯');
        convo.next();
      }
    }
    ]);
  });
});

controller.hears(['^上线时长', '^你是谁', '^你叫什么'], 'text', function (bot, message) {
  const uptime = formatUptime(process.uptime());
  bot.reply(message, '我是微信 Bot ! 我出生 ' + uptime + '了。');
});

function formatUptime(uptime) {
  let unit = '秒';
  if (uptime > 60) {
    uptime = uptime / 60;
    unit = '分钟';
  }
  if (uptime > 60) {
    uptime = uptime / 60;
    unit = '小时';
  }

  uptime = Math.floor(uptime) + ' ' + unit;
  return uptime;
}
